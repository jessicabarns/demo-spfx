declare interface ITasksManagerWebPartStrings {
  UpdateTaskLabel: string;
  DeleteMessage: string;
  No: string;
  Yes: string;
  DeleteTaskLabel: string;
  Required: string;
  SaveButton: string;
  AddTaskLabel: string;
  AllTasksTitle: string;
  MyTasksTitle: string;
  OnlyShowAssignedToMeFieldLabel: string;
  TitleColumnName: string;
  IdColumnName: string;
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
  Welcome: string;
  AssigneeColumnName: string;
}

declare module 'TasksManagerWebPartStrings' {
  const strings: ITasksManagerWebPartStrings;
  export = strings;
}
