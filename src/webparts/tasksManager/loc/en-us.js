define([], function() {
  return {
    "PropertyPaneDescription": "Description",
    "BasicGroupName": "Group Name",
    "DescriptionFieldLabel": "Description Field",
    "Welcome": "Welcome, {0}",
    "IdColumnName": "Id",
    "TitleColumnName": "Title",
    "AssigneeColumnName": "Assignee",
    "OnlyShowAssignedToMeFieldLabel": "Only show tasks assigned to me",
    "MyTasksTitle": "My tasks",
    "AllTasksTitle": "All tasks",
    "AddTaskLabel": "Add task",
    "SaveButton": "Save",
    "Required": "This field is required",
    "DeleteTaskLabel": "Delete",
    "Yes": "Yes",
    "No": "No",
    "DeleteMessage": "Do you really want to delete those items?",
    "UpdateTaskLabel": "Edit"
  }
});
