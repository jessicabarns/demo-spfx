import * as React from "react"
import {IPeoplePickerProps} from "./IPeoplePickerProps";
import {IPeoplePickerState} from "./IPeoplePickerState";
import {NormalPeoplePicker, IPersonaProps, Label} from "office-ui-fabric-react";
import {IUser} from "../../../../models/IUser";
import * as _ from "lodash";

export default class PeoplePicker extends React.Component<IPeoplePickerProps, IPeoplePickerState> {
  public render(): React.ReactElement<PeoplePicker> {
    return (
      <div>
        <Label>{this.props.label}</Label>
        <NormalPeoplePicker {...this.props}
                            defaultSelectedItems={this._formatUsersToPersonas(this.props.defaultSelected)}
                            onResolveSuggestions={this._onFilterChanged}
        />
      </div>
    );
  }

  private _onFilterChanged = (filterText: string, currentPersonas: IPersonaProps[], limitResults?: number): IPersonaProps[] => {
    if (filterText) {
      filterText = filterText.toLowerCase();
      let filteredUsers: IUser[] = _(this.props.items).filter(user => _.includes(user.Title.toLowerCase(), filterText)).uniqBy("Title").value();

      filteredUsers = limitResults ? filteredUsers.splice(0, limitResults) : filteredUsers;
      return this._formatUsersToPersonas(filteredUsers);
    } else {
      return [];
    }
  };

  private _formatUsersToPersonas(users: IUser[]): IPersonaProps[] {
    return _(users).compact().map(user => ({
      primaryText: user.Title,
      id: user.Id.toString()
    })).value();
  }
}
