import {IUser} from "../../../../models/IUser";
import {IBasePickerProps, IPersonaProps} from "office-ui-fabric-react";

export interface IPeoplePickerProps extends IBasePickerProps<IPersonaProps> {
  items: IUser[];
  label: string;
  defaultSelected: IUser[];
}
