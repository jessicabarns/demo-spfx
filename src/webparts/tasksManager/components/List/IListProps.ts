import {ITask} from "../../../../models/ITask";
import DataManager from "../DataManager";

export interface IListProps {
  dataManager: DataManager;
  items: ITask[];
  listId: string;
}
