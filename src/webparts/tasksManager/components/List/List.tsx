import * as React from 'react';
import {IListProps} from "./IListProps";
import {IListState} from "./IListState";
import {
  DetailsList,
  IColumn,
  Persona,
  PersonaSize,
  CommandBar,
  IContextualMenuItem,
  ButtonType,
  Selection
} from "office-ui-fabric-react";
import strings = require("TasksManagerWebPartStrings");
import autobind from "autobind-decorator";
import {ITask} from "../../../../models/ITask";
import Dialog from "../Dialog/Dialog";
import Form from "../Form/Form";
import {TypedHash} from "sp-pnp-js";
import {IField} from "../../../../models/IField";
import {observer} from "mobx-react";

@observer
export default class List extends React.Component<IListProps, IListState> {
  private _columns: IColumn[];
  private _dialog: Dialog;
  private _form: Form;

  constructor(props: IListProps) {
    super(props);
    this._initColumns();
    this.state = {
      selection: new Selection({onSelectionChanged: () => this.setState({selection: this.state.selection})}),
      dialog: this._renderAddForm()
    }
  }

  private _initColumns(): void {
    this._columns = [
      {
        key: 'id',
        fieldName: 'Id',
        minWidth: 100,
        name: strings.IdColumnName
      },
      {
        key: 'title',
        fieldName: 'Title',
        minWidth: 100,
        name: strings.TitleColumnName
      },
      {
        key: 'assignedTo',
        fieldName: 'AssignedTo',
        minWidth: 200,
        name: strings.AssigneeColumnName,
        onRender: this._renderAssignee
      }
    ];
  }

  @autobind
  private _renderAssignee(item: ITask, index: number, column: IColumn): React.ReactElement<HTMLElement> {
    return <Persona primaryText={item[column.fieldName].Title}
                    size={PersonaSize.extraSmall}/>;
  }

  public render(): React.ReactElement<List> {
    return (
      <div>
        <CommandBar items={this._getContextualMenuItems()}/>
        <DetailsList items={this.props.items}
                     columns={this._columns}
                     selection={this.state.selection}/>
        {this.state.dialog}
      </div>
    );
  }

  private _renderAddForm(): React.ReactElement<HTMLElement> {
    const fields: IField[] = [
      {
        key: "title",
        type: "ITextFieldProps",
        props: {
          label: strings.TitleColumnName,
          required: true,
          validateOnLoad: false
        }
      },
      {
        key: "assignedTo",
        type: "IPeoplePickerProps",
        props: {
          items: this.props.dataManager.users,
          label: strings.AssigneeColumnName,
          defaultSelected: [this.props.dataManager.currentUser],
          itemLimit: 1
        }
      }
    ];
    return <Dialog title={strings.AddTaskLabel}
                   buttons={[{
                     text: strings.SaveButton,
                     buttonType: ButtonType.primary,
                     onClick: this._submitForm
                   }]}
                   ref={(el) => this._dialog = el}>
      <Form ref={(el) => this._form = el}
            onSubmit={this._onCreateItem}
            fields={fields}
            requiredMessage={strings.Required}
            {...this.props}/>
    </Dialog>
  }

  private _renderDeleteForm(): React.ReactElement<HTMLElement> {
    return <Dialog title={strings.DeleteTaskLabel}
                   ref={el => this._dialog = el}
                   buttons={[{
                     text: strings.Yes,
                     onClick: this._onDeleteItems
                   },
                     {
                       text: strings.No,
                       buttonType: ButtonType.primary,
                       onClick: () => this._dialog.close()
                     }]}>
      {strings.DeleteMessage}
    </Dialog>
  }

  private _renderUpdateForm(): React.ReactElement<HTMLElement> {
    const selectedItem = this.state.selection.getSelection()[0] as ITask;
    const fields: IField[] = [
      {
        key: "title",
        type: "ITextFieldProps",
        props: {
          label: strings.TitleColumnName,
          required: true,
          validateOnLoad: false,
          defaultValue: selectedItem.Title
        }
      },
      {
        key: "assignedTo",
        type: "IPeoplePickerProps",
        props: {
          items: this.props.dataManager.users,
          label: strings.AssigneeColumnName,
          defaultSelected: [selectedItem.AssignedTo],
          itemLimit: 1
        }
      }
    ];
    return <Dialog title={strings.UpdateTaskLabel}
                   buttons={[{
                     text: strings.SaveButton,
                     buttonType: ButtonType.primary,
                     onClick: this._submitForm
                   }]}
                   ref={(el) => this._dialog = el}>
      <Form ref={(el) => this._form = el}
            onSubmit={this._onUpdateItem}
            fields={fields}
            requiredMessage={strings.Required}
            {...this.props}/>
    </Dialog>
  }

  private _getContextualMenuItems(): IContextualMenuItem[] {
    const contextualMenuItems: IContextualMenuItem[] = [{
      key: 'add',
      name: strings.AddTaskLabel,
      icon: 'Add',
      onClick: this._onAdd
    }];
    if (this.state.selection.count > 0) {
      contextualMenuItems.push({
        key: 'delete',
        name: strings.DeleteTaskLabel,
        icon: 'Trash',
        onClick: this._onDelete
      })
    }
    if (this.state.selection.count === 1) {
      contextualMenuItems.push({
        key: 'update',
        name: strings.UpdateTaskLabel,
        icon: 'Edit',
        onClick: this._onUpdate
      });
    }
    return contextualMenuItems;
  }

  @autobind
  private _onAdd(): void {
    this.setState({dialog: this._renderAddForm()});
    this._dialog.open();
  }

  @autobind
  private async _submitForm(): Promise<void> {
    await this._form.submit();
  }

  @autobind
  private async _onCreateItem(values: TypedHash<any>): Promise<void> {
    const assignee = values.assignedTo[0];
    const assigneeId = assignee ? (assignee.Id || parseInt(assignee.id)) : null;
    this.props.dataManager.createTask(values.title, assigneeId, this.props.listId);
    this._dialog.close();
  }

  @autobind
  private async _onUpdateItem(values: TypedHash<any>): Promise<void> {
    const assignee = values.assignedTo[0];
    const assigneeId = assignee ? (assignee.Id || parseInt(assignee.id)) : null;
    const item = this.state.selection.getSelection()[0] as ITask;
    this.props.dataManager.updateTask(item.Id, values.title, assigneeId, this.props.listId);
    this._dialog.close();
  }

  @autobind
  private _onDelete() {
    this.setState({dialog: this._renderDeleteForm()});
    this._dialog.open();
  }

  @autobind
  private _onDeleteItems(): void {
    this.props.dataManager.deleteTasks(this.state.selection.getSelection() as ITask[], this.props.listId);
    this._dialog.close();
  }

  @autobind
  private _onUpdate(): void {
    this.setState({dialog: this._renderUpdateForm()});
    this._dialog.open();
  }
}
