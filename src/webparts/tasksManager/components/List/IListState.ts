import {ISelection} from "office-ui-fabric-react";

export interface IListState {
  selection: ISelection;
  dialog: React.ReactElement<HTMLElement>;
}
