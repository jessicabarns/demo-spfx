import * as React from 'react';
import {ITasksManagerContainerProps} from "./ITasksManagerContainerProps";
import styles from "./TasksManagerContainer.module.scss";
import {Text} from "@microsoft/sp-core-library";
import strings = require("TasksManagerWebPartStrings");
import List from "../List/List";
import {ITaskManagerContainerState} from "./ITaskManagerContainerState";
import {ITask} from "../../../../models/ITask";
import * as _ from "lodash";
import {observer} from "mobx-react";
import {Spinner, SpinnerSize} from "office-ui-fabric-react";

@observer
export default class TasksManagerContainer extends React.Component<ITasksManagerContainerProps, ITaskManagerContainerState> {
  private _list: List;

  constructor(props: ITasksManagerContainerProps) {
    super(props);
    this.state = {
      onlyShowAssignedToMe: props.onlyShowAssignedToMe
    }
  }

  public componentDidMount(): void {
    this.props.dataManager.fetchItemsInList(this.props.listId);
  }

  public componentWillReceiveProps(nextProps: ITasksManagerContainerProps): void {
    if (this.props.onlyShowAssignedToMe !== nextProps.onlyShowAssignedToMe) {
      this.setState({
        onlyShowAssignedToMe: nextProps.onlyShowAssignedToMe
      });
    }
  }

  public render(): React.ReactElement<ITasksManagerContainerProps> {
    return (
      <div className={styles.tasksManager}>
        <div className={styles.container}>
          <div className={styles.row}>
            <div className={styles.column}>
              <span
                className={styles.title}>{Text.format(strings.Welcome, this.props.dataManager.getCurrentUserName())}</span>
              <span
                className={styles.title}>{this.state.onlyShowAssignedToMe ? strings.MyTasksTitle : strings.AllTasksTitle}</span>
              {this.props.dataManager.isLoading && <Spinner size={SpinnerSize.large}/>}
              <List {...this.props}
                    items={this._getItemsToShow()}
                    ref={el => this._list = el}/>
            </div>
          </div>
        </div>
      </div>
    );
  }

  private _getItemsToShow(): ITask[] {
    return this.state.onlyShowAssignedToMe ?
      _.filter(this.props.dataManager.items, {AssignedToId: _.get(this.props.dataManager.currentUser, "Id", -1)}) :
      this.props.dataManager.items;
  }
}
