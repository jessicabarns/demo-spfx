import DataManager from "../DataManager";

export interface ITasksManagerContainerProps {
  dataManager: DataManager;
  listId: string;
  onlyShowAssignedToMe: boolean;
}
