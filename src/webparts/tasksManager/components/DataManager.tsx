import {IWebPartContext} from "@microsoft/sp-webpart-base";
import {IObservableArray, observable} from "mobx";
import {ITask} from "../../../models/ITask";
import {IUser} from "../../../models/IUser";
import * as _ from "lodash";
import ListDataProvider from "../../../dataProviders/ListDataProvider";

export default class DataManager {
  private _context: IWebPartContext;
  private _dataProvider: ListDataProvider;

  @observable public items: ITask[] = [];
  @observable public users: IUser[] = [];
  @observable public currentUser: IUser;
  @observable public isLoading: boolean;

  constructor(context: IWebPartContext, dataProvider: ListDataProvider) {
    this._context = context;
    this._dataProvider = dataProvider;
    this._init();
  }

  private async _init(): Promise<void> {
    this.isLoading = true;
    this.users = await this._dataProvider.getAllUsers();
    this.currentUser = await this._dataProvider.getCurrentUser();
    this.isLoading = false;
  }

  public async fetchItemsInList(listId: string): Promise<void> {
    this.isLoading = true;
    const items = await this._dataProvider.getItemsInList(listId);
    const assigneesIds = _(items).map("AssignedToId").compact().uniq().value() as number[];
    const assignees = await Promise.all(assigneesIds.map(async id => await this._dataProvider.getUserById(id)));
    items.forEach(item => item.AssignedTo = _.find(assignees, {Id: item.AssignedToId}));
    this.items = items;
    this.isLoading = false;
  }

  public getCurrentUserName(): string {
    return this._context.pageContext.user.displayName;
  }

  public async createTask(title: string, assigneeId: number, listId: string): Promise<void> {
    this.isLoading = true;
    const item = await this._dataProvider.addItemToList(listId, {
      Title: title,
      AssignedToId: assigneeId
    });
    item.data.AssignedTo = _.find(this.users, {Id: assigneeId});
    this.items = _.concat((this.items as IObservableArray<ITask>).peek(), [item.data]);
    this.isLoading = false;
  }

  public async deleteTasks(items: ITask[], listId: string): Promise<void> {
    this.isLoading = true;
    const itemsIds = _.map(items, "Id") as number[];
    await this._dataProvider.deleteItemsFromList(itemsIds, listId);

    const observableItems = (this.items as IObservableArray<ITask>);
    this.items = _.filter(observableItems, item => !_.find(itemsIds, id => id === item.Id));
    this.isLoading = false;
  }

  public async updateTask(id: number, title: string, assigneeId: number, listId: string): Promise<void> {
    this.isLoading = true;
    await this._dataProvider.updateItemInList(id, listId, {
      Title: title,
      AssignedToId: assigneeId
    });

    const items = (this.items as IObservableArray<ITask>).peek();
    const index = _.findIndex(items, {Id: id});
    items[index].Title = title;
    items[index].AssignedTo = _.find(this.users, {Id: assigneeId});

    this.items = items;

    this.isLoading = false;
  }
}
