import {IButtonProps} from "office-ui-fabric-react";

export interface IDialogState {
  hidden: boolean;
  buttons?: IButtonProps[];
}
