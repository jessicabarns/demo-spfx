import {IDialogProps} from "./IDialogProps";
import {IDialogState} from "./IDialogState";
import * as _ from 'lodash';
import {
  Dialog as FabricDialog,
  DialogType,
  DialogFooter,
  DefaultButton,
  ButtonType,
  PrimaryButton
} from 'office-ui-fabric-react/lib';
import * as React from "react";
import autobind from "autobind-decorator";

export default class Dialog extends React.Component<IDialogProps, IDialogState> {
  constructor(props: IDialogProps) {
    super(props);
    this.state = {
      hidden: !this.props.visible
    };
  }

  public render(): React.ReactElement<IDialogProps> {
    return (
      <FabricDialog
        hidden={this.state.hidden}
        onDismiss={this.close}
        dialogContentProps={{
          type: DialogType.normal,
          title: this.props.title
        }}
      >
        {this.props.children}
        <DialogFooter>
          {this._renderButtons()}
        </DialogFooter>
      </FabricDialog>
    );
  }

  @autobind
  public close(): void {
    if (this.props.onDismiss) {
      this.props.onDismiss();
    }
    this.setState({hidden: true});
  }

  public open() {
    this.setState({hidden: false});
  }

  private _renderButtons() {
    return _.map(this.props.buttons, (button) => {
      let buttonElement;
      switch (button.buttonType) {
        case(ButtonType.primary):
          buttonElement = <PrimaryButton {...button}/>;
          break;

        default:
          buttonElement = <DefaultButton {...button}/>;
          break;
      }
      return buttonElement;
    });
  }
}
