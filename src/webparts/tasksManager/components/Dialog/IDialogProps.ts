import {IButtonProps} from "office-ui-fabric-react/lib";

export interface IDialogProps {
  title: string;
  buttons: IButtonProps[];
  visible?: boolean;
  onSave?: () => void;
  onDismiss?: () => void;
}
