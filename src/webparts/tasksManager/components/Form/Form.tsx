import * as React from "react";
import * as _ from "lodash";
import {
  TextField,
  ITextFieldProps,
  IDropdownProps,
  Dropdown,
  IDropdownOption,
  IPersonaProps
} from "office-ui-fabric-react/lib";
import {TypedHash} from "sp-pnp-js";
import {IFormProps} from "./IFormProps";
import {IFormState} from "./IFormState";
import {IField} from "../../../../models/IField";
import {IPeoplePickerProps} from "../PeoplePicker/IPeoplePickerProps";
import PeoplePicker from "../PeoplePicker/PeoplePicker";
import autobind from "autobind-decorator";

export default class Form extends React.Component<IFormProps, IFormState> {
  private _form: HTMLElement;
  private _values: TypedHash<any>;
  private _errors: TypedHash<string>;
  private _fields: TypedHash<TextField | Dropdown>;

  constructor(props: IFormProps) {
    super(props);
    this._values = {};
    this._errors = {};
    this._fields = {};
  }

  public async componentWillMount(): Promise<void> {
    this._initFields(this.props);
    this._initValues(this.props);
  }

  public async componentWillReceiveProps(nextProps: IFormProps): Promise<void> {
    this._initFields(nextProps);
    this._initValues(nextProps);
  }

  private _initFields(props: IFormProps): void {
    _.forEach(props.fields, (field: IField) => {
      switch (field.type) {
        case "ITextFieldProps":
          const textFieldProps = field.props as ITextFieldProps;
          textFieldProps.onChanged = (newValue) => {
            this._onChanged(newValue, field.key);
          };

          const onGetErrorMessage = textFieldProps.onGetErrorMessage;
          if (onGetErrorMessage || textFieldProps.required) {
            textFieldProps.onGetErrorMessage = async (value: string) => {
              if (textFieldProps.required) {
                const error = value ? "" : props.requiredMessage;
                return this._onGetErrorMessage(error, field.key);
              }
              else if (onGetErrorMessage) {
                const error = await onGetErrorMessage(value);
                return this._onGetErrorMessage(error, field.key);
              }
            };
          }
          break;

        case "IDropdownProps":
          const dropdownProps = field.props as IDropdownProps;
          dropdownProps.onChanged = (item: IDropdownOption) => {
            this._onChanged(item, field.key);
          };
          break;

        case "IPeoplePickerProps":
          const peoplePickerProps = field.props as IPeoplePickerProps;
          peoplePickerProps.onChange = (items: IPersonaProps[]) => {
            this._onChanged(items, field.key);
          };
          break;
      }
    });
  }

  private _onGetErrorMessage(error: string, fieldName: string): string {
    return error ? this._addError(fieldName, error) : this._removeError(fieldName);
  }

  private _initValues(props: IFormProps): void {
    _.forEach(props.fields, (field) => {
      switch (field.type) {
        case "ITextFieldProps":
          const textFieldProps = field.props as ITextFieldProps;
          this._values[field.key] = textFieldProps.defaultValue;
          break;
        case "IDropdownProps":
          const dropdownProps = field.props as IDropdownProps;
          this._values[field.key] = _.find(dropdownProps.options, {key: dropdownProps.defaultSelectedKey});
          break;
        case "IPeoplePickerProps":
          const peoplePickerProps = field.props as IPeoplePickerProps;
          this._values[field.key] = peoplePickerProps.defaultSelected;
          break;
      }
    });
  }

  public render(): React.ReactElement<IFormProps> {
    return (
      <form onSubmit={this._onSubmit}
            ref={(el) => this._form = el}>
        {this._renderFields()}
      </form>
    );
  }

  private _renderFields(): React.ReactElement<HTMLElement>[] {
    return _.map(this.props.fields, (field) => {
      let element;
      switch (field.type) {
        case "ITextFieldProps":
          const textFieldProps = field.props as ITextFieldProps;
          element = <TextField {...textFieldProps}
                               ref={(el) => this._fields[field.key] = el}/>;
          break;
        case "IDropdownProps":
          const dropdownProps = field.props as IDropdownProps;
          element = <Dropdown {...dropdownProps}
                              ref={(el) => this._fields[field.key] = el}/>;
          break;
        case "IPeoplePickerProps":
          const peoplePickerProps = field.props as IPeoplePickerProps;
          element = <PeoplePicker {...peoplePickerProps}/>;
          break;
      }
      return element;
    });
  }

  public async submit(): Promise<void> {
    await this._onSubmit(null);
  }

  private _onChanged(newValue: any, fieldName: string): void {
    if (typeof newValue === "string") {
      newValue = _.trim(newValue);
    }
    this._values[fieldName] = newValue;
  }

  @autobind
  private async _onSubmit(event): Promise<void> {
    if (event) {
      event.preventDefault();
    }

    await this._validateForm();
    if (!this._hasErrors() && !this._isFormEmpty()) {
      this.props.onSubmit(this._values);
    }
  }

  private async _validateForm(): Promise<void> {
    await Promise.all(_.map(this.props.fields, field => this._validateField(field)));
  }

  private async _validateField(field: IField): Promise<void> {
    let props;
    switch (field.type) {
      case "ITextFieldProps":
        props = field.props as ITextFieldProps;
        break;

      case "IDropdownProps":
        props = field.props as IDropdownProps;
        break;

      case "IPeoplePickerProps":
        props = field.props as IPeoplePickerProps;
        break;
    }
    if (props.onGetErrorMessage) {
      let error = await props.onGetErrorMessage(this._values[field.key]);
      (this._fields[field.key] as TextField).setState({errorMessage: error});
    }
  }

  private _addError(fieldName: string, error: string): string {
    this._errors[fieldName] = error;
    if (this.props.onError) {
      this.props.onError();
    }
    return error;
  }

  private _removeError(fieldName: string): string {
    delete this._errors[fieldName];
    if (this.props.onNoError && !this._hasErrors()) {
      this.props.onNoError();
    }
    return "";
  }

  private _isFormEmpty(): boolean {
    let isEmpty = true;
    _.forEach(_.keys(this._values), (key) => {
      if (!this._isFieldEmpty(key)) {
        isEmpty = false;
      }
    });
    return isEmpty;
  }

  private _isFieldEmpty(fieldName: string): boolean {
    let value = this._values[fieldName];
    return _.isEmpty(value);
  }

  private _hasErrors(): boolean {
    return _.keys(this._errors).length > 0;
  }
}
