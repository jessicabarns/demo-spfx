import {TypedHash} from "sp-pnp-js";
import {IField} from "../../../../models/IField";

export interface IFormProps {
  onSubmit: (values: TypedHash<string>) => void;
  fields: IField[];
  onError?: () => void;
  onNoError?: () => void;
  requiredMessage: string;
}
