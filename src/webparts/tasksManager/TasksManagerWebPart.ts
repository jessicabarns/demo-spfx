import * as React from 'react';
import * as ReactDom from 'react-dom';
import {Version} from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration, PropertyPaneToggle
} from '@microsoft/sp-webpart-base';
import * as strings from 'TasksManagerWebPartStrings';
import {ITasksManagerContainerProps} from "./components/TasksManagerContainer/ITasksManagerContainerProps";
import TasksManagerContainer from "./components/TasksManagerContainer/TasksManagerContainer";
import ListDataProvider from "../../dataProviders/ListDataProvider";
import DataManager from "./components/DataManager";

export interface ITasksManagerWebPartProps {
  listId: string;
  onlyShowAssignedToMe: boolean;
}

export default class TasksManagerWebPart extends BaseClientSideWebPart<ITasksManagerWebPartProps> {
  private _dataProvider: ListDataProvider;
  private _dataManager: DataManager;

  public async onInit(): Promise<void> {
    this._dataProvider = new ListDataProvider(this.context);
    this._dataManager = new DataManager(this.context, this._dataProvider);
  }

  public render(): void {
    const element: React.ReactElement<ITasksManagerContainerProps> = React.createElement(
      TasksManagerContainer,
      {
        ...this.properties,
        dataManager: this._dataManager
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneToggle("onlyShowAssignedToMe", {
                  label: strings.OnlyShowAssignedToMeFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
