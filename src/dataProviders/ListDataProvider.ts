import {IWebPartContext} from "@microsoft/sp-webpart-base";
import * as pnp from 'sp-pnp-js';
import {IUser} from "../models/IUser";
import {ITask} from "../models/ITask";
import {ItemAddResult, TypedHash} from "sp-pnp-js";

export default class ListDataProvider {
  private _context: IWebPartContext;

  constructor(context: IWebPartContext) {
    this._context = context;
    pnp.setup({
      spfxContext: context
    });
  }

  public async getItemsInList(listId: string): Promise<ITask[]> {
    return pnp.sp.web.lists.getById(listId).items.get();
  }

  public async getUserById(id: number): Promise<IUser> {
    return pnp.sp.web.getUserById(id).get();
  }

  public async getCurrentUser(): Promise<IUser> {
    const result = await pnp.sp.site.rootWeb.ensureUser(this._context.pageContext.user.email);
    return result.data;
  }

  public async getAllUsers(): Promise<IUser[]> {
    return pnp.sp.web.siteUsers.get();
  }

  public async addItemToList(listId: string, properties: TypedHash<any>): Promise<ItemAddResult> {
    return pnp.sp.web.lists.getById(listId).items.add(properties);
  }

  public async deleteItemsFromList(itemsIds: number[], listId: string): Promise<void> {
    Promise.all(itemsIds.map(async id => await pnp.sp.web.lists.getById(listId).items.getById(id).delete()));
  }

  public async updateItemInList(id: number, listId: string, properties: TypedHash<any>): Promise<void> {
    pnp.sp.web.lists.getById(listId).items.getById(id).update(properties);
  }
}
