import {IUser} from "./IUser";

export interface ITask {
  Id: number;
  Title: string;
  AssignedToId: number;
  AssignedTo: IUser;
}
