import {ITextFieldProps, IDropdownProps} from "office-ui-fabric-react/lib";
import {IPeoplePickerProps} from "../webparts/tasksManager/components/PeoplePicker/IPeoplePickerProps";

export interface IField {
  type: "ITextFieldProps" | "IDropdownProps" | "IPeoplePickerProps";
  props: ITextFieldProps | IDropdownProps | IPeoplePickerProps;
  key: string;
}
